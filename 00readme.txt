/*! \file 00readme.txt
 *
 *  \brief Library for Dallas/Maxxim DS1821 Digital Thermostat
 *
 *
 * libDS1821 provides basic functions for reading the Dallas/Maxxim
 * DS1821 Programmable Digital Thermostat.  The library is specific to
 * the PIC24F family of microcontrollers, and specifically, those models
 * having only PORTA and PORTB, or circuits to which the DS1821 is only
 * connected to PORTA or PORTB.
 *
 * The DS1821 uses the Dallas "One Wire" protocol.  This is an open drain,
 * single wire bus on which commands are sent and results received over the
 * same wire.  There is no addressing scheme; only one DS1821 may be on
 * the wire (in contrast to I2C where devices are addressed and multiple
 * devices may share the bus.)
 *
 * The communication with the DS1821 proceeds as follows:  First, the PIC
 * sends a "Master Reset" pulse to the DS1821 by holding the line low for
 * 600 microseconds.  It then releases the line and approximately 30 us
 * later the DS1821 will announce its presence by holding the line low
 * for 120 us.
 *
 * An 8 bit command is then sent to the DS1821, low order bit first. Each
 * bit resides in a 60 microsecond "time slot".  The beginning of the bit
 * is announced by holding the line low for 7us.  The line is raised if
 * the bit is a 1, and held low for the remainder of the time slot if the
 * desired bit is 0.
 *
 * The DS1821 can only send data in response to a read command from the
 * PIC.  The protocol is the same, except that the PIC initiates each
 * time slot by holding the line down for 1 microsecond.
 *
 * The library maintains a list of up to eight sensors, numbered 0 to 7.
 * The user assigns a DS1821 number to a port and bit with the
 * DS1821_Initialize() function.  Thereafter, each call includes a
 * sensor number.
 *
 * The library relies on the relationship in memory between the various
 * registers associated with a PORT.  On the PIC24FV16KM202, the port-related
 * registers, with the exception of analog select, are contiguous in
 * memory.
 *
\dot
digraph Port {
	rankdir="RL";
    fontname="Helvetica-Bold";
    label="Registers associated with a PORT";
	
Port [shape=none, margin=0, fontname="Helvetica" label=<
      <TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="4">
      <TR>
      <TD ROWSPAN="5" BGCOLOR="burlywood"><FONT COLOR="black"><B>PIC24<BR/>Port</B></FONT></TD>
      <TD BGCOLOR="navajowhite">TRISx</TD>
      <TD BGCOLOR="navajowhite"><FONT COLOR="gray">02Cx</FONT></TD>
      <TD BGCOLOR="navajowhite">Data direction</TD>
      </TR>
      <TR>
      <TD BGCOLOR="wheat">PORTx</TD>
      <TD BGCOLOR="wheat"><FONT COLOR="gray">02Cx+2</FONT></TD>
      <TD BGCOLOR="wheat">Input data</TD>
      </TR>
      <TR>
      <TD BGCOLOR="navajowhite">LATx</TD>
      <TD BGCOLOR="navajowhite"><FONT COLOR="gray">02Cx+4</FONT></TD>
      <TD BGCOLOR="navajowhite">Output data</TD>
      </TR>
      <TR>
      <TD BGCOLOR="wheat">ODCx</TD>
      <TD BGCOLOR="wheat"><FONT COLOR="gray">02Cx+6</FONT></TD>
      <TD BGCOLOR="wheat">Open drain</TD>
      </TR>
      <TR>
      <TD BGCOLOR="lightgoldenrod"><b>ANSx</b></TD>
      <TD BGCOLOR="lightgoldenrod"><FONT COLOR="gray">04Ex</FONT></TD>
      <TD BGCOLOR="navajowhite">Analog Select</TD>
      </TR>
      </TABLE>>];
}
\enddot
 * (The interrupt on change registers, CNENx, CNPUx and CNPDx, also have
 * one bit for each pin, however, unlike the PIC32, these bits do not map
 * to the PORT pins in a one-to-one fashion like LAT, TRIS, ODC and ANS,
 * but instead are numbered 1 to 30, with missing numbers, and are mapped
 * to pins according to a table in the datasheet.)
 *
 * The library stores the address of the PORT register and a mask for the
 * bit in static arrays.  That address can be used as a pointer to manipulate
 * the port.  The ANSx registers are more problematic, however they only
 * need to be addressed during initialization, so the fact that this register
 * is not contiguous is not especially burdensome.
 *
 * Because of this flexibility, the code for setting or clearing a bit is
 * a little arcane.  The contents of the address from the remembered port
 * addresses is ORed with the bit mask to set a bit, or ANDed with the
 * complement of the bit mask to clear a bit.
 *
 * To set a bit in the latch, for example:
\code
  *(uDSport[nDSnum]+1) |= uDSmask[nDSnum];
\endcode
 * (The latch is at the port address +2, but since it is a 2 byte
 * pointer, +1 adds 2).
 *
 * To clear a bit:
\code
    uImask = uDSmask[nDSnum] ^ 0xffff;
    *(uDSport[nDSnum]+1) &= uImask;
\endcode
 *
 * The library uses TMR1, the only unencumbered timer on the
 * PIC24FV16KM202.
 *
 *
 */
