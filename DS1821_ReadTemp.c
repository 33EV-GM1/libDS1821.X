/*! \file  DS1821_ReadTemp.c
 *
 *  \brief Read the current temperature from the DS1821
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_ReadTemp - Get the current temperature from the DS1821  */
/*! DS1821_ReadTemp() - Sets the DS1821 to one shot mode. Then commands
 *  it to begin conversion.  The function then polls the sensor for
 *  completion, and when complete, reads the result.
 *
 * Pseudocode:
 * \code
 * Send master reset
 * Send DS_WRITESTATUS
 * Send DS_CONFIG | DS_ONESHOT
 * Send master reset
 * SendDS_STARTCONV
 * Call DS1821_PollStatus to wait for competion
 * Send master reset
 * Send DS_READTEMP
 * Read the result
 * Return the result
 * \endcode
 * \callgraph
 *
 * \param nDSnum int - number of the DS121 to read
 * \return char - Centigrade temperature
 */
char DS1821_ReadTemp(int nDSnum)
{
    char result;

    /* Set the DS1821 into one-shot mode */
    DS1821_MasterReset(nDSnum);
    DS1821_Command( nDSnum, DS_WRITESTATUS );
    DS1821_Command( nDSnum, DS_CONFIG | DS_ONESHOT );

    /* Instruct the DS1821 to begin a temperature conversion */
    DS1821_MasterReset(nDSnum);
    DS1821_Command( nDSnum, DS_STARTCONV );

    /* Wait for the conversion to complete */
    DS1821_PollStatus(nDSnum);

    /* Send read temperature command */
    DS1821_MasterReset(nDSnum);
    DS1821_Command( nDSnum, DS_READTEMP );

    /* Read the result */
    result = (char)DS1821_Read( nDSnum );
    return result;

}
