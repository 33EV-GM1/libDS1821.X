/*! \file  DS1821_Read.c
 *
 *  \brief Read a byte from the DS1821
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:23 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_Read - Read a byte from the DS1821 */

/*! DS1821_Read() - Reads a byte from the DS1821.
 *  First calculate the inverted mask.  Then set the
 *  result to all zeroes.  For each bit shift the
 *  result right one bit, read the DS1821, and if the
 *  result is true, OR in 0x80 to the result.
 *
 * Pseudocode:
 * \code
 * Calculate inverted mask
 * Set result to zero
 * for i=0..7
 *     Shift result right one bit
 *     Clear the LAT bit
 *     Initialize the timer to 1us
 *     Wait for timer to expire
 *     Set the LAT bit
 *     Reset the timer to 60us
 *     Wait for 7us to expire
 *     Test the PORT bit
 *     if the PORT bit is true
 *         result |= 0x80
 *     Wait for the timer to expire
 * end for
 * return result
 * \endcode
 * \callergraph
 *
 * \param nDSnum int - Number of the DS1821 to read
 * \return unsigned char - Byte read from DS1821
 */
unsigned char DS1821_Read(int nDSnum )
{
    int i,result;
    unsigned int uImask;
    
    // Calculate the inverted mask
    uImask = uDSmask[nDSnum] ^ 0xffff;

    result = 0;
    for ( i=0; i<8; i++ )
	{
	    /* Move to the next bit */
	    result = result >> 1;

	    /* Send a read pulse to the DS1821 */
	    *(uDSport[nDSnum]+1) &= uImask;
	    PR1821 = T1US;
	    TMR1821 = 0;
	    IF1821 = 0;
	    while (!IF1821)
		;

            /* Set the LATCH high */
	    *(uDSport[nDSnum]+1) |= uDSmask[nDSnum];

	    /* Reset the timer - The read time slot is 60us, however, the 1821
	     * will respond between 1 and 15 us, so we will watch the timer,
             * sample at 7us, and then allow the full 60us to expire before
             * moving on  */
	    PR1821 = T60US;
	    TMR1821 = 0;
	    IF1821 = 0;

	    /* Wait a bit for the DS1821 to respond */
	    while (TMR1821 < T7US)
		;

	    /* Now sample the DS1821 */
	    if ( (*(uDSport[nDSnum])&uDSmask[nDSnum]) )
		result |= 0x80;

	    /* Wait for the time slot to expire */
	    while ( !IF1821 )
		;

	}
    return (unsigned char)result;
}
