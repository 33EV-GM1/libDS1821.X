/*! \file  DS1821_Command.c
 *
 *  \brief Send a command to the DS1821
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:27 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_Command - Send a command to the DS1821 */

/*! DS1821_Command() sends an 8 bit command to the DS1821.
 *
 * First an inverted mask is created, then the command passed in
 * as a parameter is sent out to the pin, low order bit first.
 *
 * Pseudocode:
 * \code
 * Calculate inverted mask, uImask
 * for i=0 .. 7
 *     Set the timer period register to 60us
 *     Clear the timer and timer interrupt flag
 *     Lower the port pin
 *     Wait for the timer to reach 7us
 *     if the rightmost bit of the command is 1, raise the pin
 *     Wait for the timer interrupt flag to be set
 *     Raise the port pin
 *     Shift the command one bit to the right
 * end for
 * Set the timer to 1us
 * Clear the timer and interrupt flag
 * Wait for the interrupt flag
 * \endcode
 * \callergraph
 *
 * \param nDSnum int - Number of the DS1821 to send a command to
 * \param command unsigned char - Command to send
 */
void DS1821_Command( int nDSnum, unsigned char command )
{
    int i;
  unsigned int uImask;

  // Calculate the inverted mask
  uImask = uDSmask[nDSnum] ^ 0xffff;


    for ( i=0; i<8; i++ )
	{
	    /* Reset the timer */
	    PR1821 = T60US;
	    TMR1821 = 0;
	    IF1821 = 0;

	    /* Pull down the pin */
            *(uDSport[nDSnum]+1) &= uImask;

	    /* Wait for 7 us to expire */
	    /* Master must keep bus low for>1us and <15us */
	    while ( TMR1821<T7US )
		;
	    if ( command & 0x01 )
                *(uDSport[nDSnum]+1) |= uDSmask[nDSnum];
	    /* Wait for the time slot to expire */
	    while ( !IF1821 )
		;

	    /* Reset the bus */
	    *(uDSport[nDSnum]+1) |= uDSmask[nDSnum];

	    /* Next bit */
	    command = command >> 1;
	}
    /* Wait 1 microcsecond */
    PR1821 = T1US;
    TMR1821 = 0;
    IF1821 = 0;
    while ( !IF1821 )
        ;
}
