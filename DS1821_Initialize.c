/*! \file  DS1821_Initialize.c
 *
 *  \brief Initialize the pin and timer for the DS1821 digital thermostat
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:32 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#define EXTERN
#include "DS1821.h"

/*! DS1821_Initialize - Initialization for the DS1821 */
/*! DS1821_Initialize() - Remember the port address in an array and create
 * another array entry containing a bit set corresponding to the bit
 * number passed as a parameter.  Set the DS1821 port latch high and set the
 * port to be an input.  (It will later be set to be an output, but it must
 *  be already high when that happens.)  Also sets it to digital and
 *  open drain.
 *
 *  Then initialize the timer that will be used.
 *
 * Pseudocode:
 * \code
 * Remember port name in uDSport array
 * Create mask for pin in uDSmask array
 * Calculate inverted mask
 * Set the output latch high
 * Set the output pin to an output
 * Set the output pin to open drain
 * If the port address is equal to the PORTA address
 *     AND ANSA with the inverted mask
 * else
 *     AND ANSB with the inverted mask
 * Initialize the timer to Fosc, 1:1 prescale
 * \endcode
 *
 * \param nDSnum int - Number to assign to this DS1821
 * \param uDSportname unsigned int * - Address of the PORT to which this DS1821 is attached
 * \param nDSpin int - Bit number on the port to which this DS1821 is attached
 * \return none
 */
void DS1821_Initialize( int nDSnum, volatile unsigned int *uDSportname, int nDSpin )
{
  unsigned int uImask;

  uDSport[nDSnum] = (unsigned int *)uDSportname;
  uDSmask[nDSnum] = 1<<nDSpin;

  // Calculate the inverted mask
  uImask = uDSmask[nDSnum] ^ 0xffff;

    // Set the DS1821 latch to high and make it an open-drain output
  // Set true to allow float before changing
  *(uDSport[nDSnum]+1) |= uDSmask[nDSnum];
  // Make the pin an output
  *(uDSport[nDSnum]-1) &= uImask;
  // Set the pin to open drain
  *(uDSport[nDSnum]+2) |= uDSmask[nDSnum];
  // Set the pin to digital
  if ( uDSportname == (&PORTA) )
    ANSA &= uImask;
  else
    ANSB &= uImask;

  /* Set up timer for DS1821                                              */
  TMR1821 = 0;                /* Clear timer                              */
  PR1821  = 0xfffe;           /* Period register to a large number        */
  CON1821 = 0x8000;           /* Fosc/4, 1:1 prescale, start timer        */
}
